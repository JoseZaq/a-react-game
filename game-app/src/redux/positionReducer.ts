import {createSlice} from "@reduxjs/toolkit";

export interface positionState{
    playerPosition: object,
}
export const initialState = {
    playerPosition: {x:0,y:0},
}
export const positionSlice = createSlice({
    name:'positions',
    initialState,
    reducers:{
        sumPlayerX: (state, action) =>{state.playerPosition.x += action.payload},
        sumPlayerY: (state, action) =>{state.playerPosition.y += action.payload},
        setPlayerX: (state, action) =>{state.playerPosition.x = action.payload},
        setPlayerY: (state, action) =>{state.playerPosition.y = action.payload}
    }
})
export const { sumPlayerX, sumPlayerY,setPlayerX,setPlayerY } = positionSlice.actions
 export default positionSlice.reducer