import { createSlice, PayloadAction} from "@reduxjs/toolkit";
let layer0 = require('../jsons/layer0.json')
let layer1 = require('../jsons/layer1.json')
export interface mapState {
    layer0: any[],
    layer1: any[],
    solidMap:any[],
    pixels: number,
    x0:number,
    y0:number,
}
const initialState: mapState ={
    layer0: layer0,
    layer1:layer1,
    solidMap:[],
    pixels: 32,
    x0:0,
    y0:0,
}

export const mapSlice = createSlice({
    name:'map',
    initialState,
    reducers:{
        setSolidMap: (state,action) =>{
            state.solidMap = action.payload
        },
        setPixels:(state,action) => {
            state.pixels=action.payload
        },
        setWorldPosition:(state,action)=>{
            state.x0=action.payload.x
            state.y0=action.payload.y
        }
    }
})

export const { setSolidMap, setPixels,setWorldPosition } = mapSlice.actions
export default mapSlice.reducer