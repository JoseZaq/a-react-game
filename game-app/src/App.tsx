import Player from "./components/player/player";
import World from "./components/world/world";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {setSolidMap} from "./redux/mapReducer";
import {RootState} from "./redux/store";
function App() {
    const dispatch = useDispatch()
    const {layer0,layer1,solidMap} = useSelector((state : RootState) => state.map)
    useEffect(() => {
        dispatch(setSolidMap(makeSolidMap(layer0,layer1)))
        console.log(solidMap)
    },[])
    function makeSolidMap(obj1:any,obj2:any){
        let solidMatrix:any[] = []
        obj1.map((row:any,y:number) => {
            let _row:any[]=[]
            row.map((e:any,x:number) => {
                e.solid ? _row.push(true): obj2[y][x].solid ? _row.push(true) : _row.push(false)
            })
            solidMatrix.push(_row)
        })
        return solidMatrix
    }
    return (
        <div className="App" style={{
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            position:'absolute',
            width: '-webkit-fill-available',
            height: '-webkit-fill-available',
            background:'radial-gradient(#2f2a2a,#000000,black)',
        }} >
            <World/>
        </div>
    );
}

export default App;
