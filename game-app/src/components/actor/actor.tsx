import Sprite from '../sprite/sprite'
type props = {
    sprite: string,
    data: any,
    position: any,
    step: number,
    dir:number,
}
export default function Actor({ sprite, data , position={x:0 , y:0}, step = 0, dir = 0}:props) {
    const { h, w } = data
    return (
        <Sprite
            image={sprite}
            data={{
                x: step * w,
                y: dir * h,
                h,
                w,
            }}
            position={position}
        />
    )
}