import {useSelector} from "react-redux";
import {RootState} from '../../redux/store'
type props = {
    layer:any,
    pixels:number,
}
export default function Layer({layer, pixels}:props){
    return(
        <div style={{position:'absolute',}}>
            {layer.map((row:any,y:number) =>
                <div key={y} style={{display:'flex'}} >
                {
                    row.map((tile:any, x:number) =>(
                        <div key={x} id={tile.id} style={
                            {
                                display:'flex',
                                background: `url(/sprites/${tile.background}.png) -${tile.x}px -${tile.y}px no-repeat`,
                                width:pixels,
                                height:pixels,
                            }
                        }/>
                    ))
                }
            </div>)}
        </div>
    )
}