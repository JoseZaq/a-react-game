type props = {
    image:string,
    data:any,
    position:any,
}
export default function Sprite({ image, data, position}:props){
    const {x,y, h, w} = data;
    return( <div
     style={{
         position:"absolute",
         top: position.y,
         left: position.x,
         height: `${h}px`,
         width: `${w}px`,
         backgroundImage: `url(${image})`,
         backgroundRepeat: "no-repeat",
         backgroundPosition: `-${x}px -${y}px`, 
     }}>
    </div>)
}