import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../redux/store";
import Layer from "../layer/layer";
import Player from "../player/player";
import {setWorldPosition} from "../../redux/mapReducer";

export default function World(){
    //vars
    const dispatch = useDispatch()
    const pixels=32
    const {layer0,layer1} = useSelector((state : RootState) => state.map)

    const yWorldPosition = (window.innerHeight-29) /2 - (layer0.length * pixels)/2
    const xWorldPosition = (window.innerWidth-5)/2 - (layer0[0].length * pixels)/2
    dispatch((setWorldPosition({x: xWorldPosition ,y:yWorldPosition})))
    //
    return(
        <>
            <Layer layer={layer0}  pixels={pixels} />
            <Layer layer={layer1} pixels={pixels} />
            <Player skin="m1" x={xWorldPosition} y={yWorldPosition}/>
        </>
    )
}