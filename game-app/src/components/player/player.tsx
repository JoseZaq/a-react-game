import Actor from "../actor/actor";
import useKeyPress from "../../hooks/use-key-press/index";
import useWalk from "../../hooks/use-walk/useWalk";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../redux/store";
import {sumPlayerX, sumPlayerY} from "../../redux/positionReducer";
import {useEffect} from "react";
import IsASolidTile from "../../hooks/isASolidTile";
type props={
    skin:any,
    x:number,
    y:number,
}
export default function Player({ skin,x,y }:props) {
    // vars
    const dispatch= useDispatch();
    const {pixels} = useSelector((state:RootState )=> state.map)
    /// inicializar posicion
    useEffect(() =>{
        dispatch(sumPlayerX(x+pixels*2))
        dispatch(sumPlayerY(y+pixels*3))

    },[])
    ///
    const {playerPosition} = useSelector((state : RootState) => state.positions)
    const data = {
        h: 32,
        w: 32
    };
    // hooks
    /// useWalk
    const {walk, dir, step, direction} = useWalk(3);
    /// use-key-press
    useKeyPress((e:any) => {
        const dir = e.key.replace("Arrow", "").toLowerCase();// direccion del player, muestra la flecha pulsada
        if(direction.hasOwnProperty(dir) ){
            walk(dir);
        }
        e.preventDefault();
    })
    return (
        <Actor sprite={`/sprites/${skin}.png`} data={data} position={playerPosition} step={step}  dir={dir} />
    )
}