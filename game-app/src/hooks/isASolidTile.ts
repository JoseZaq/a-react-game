import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../redux/store";
export default function IsASolidTile(x:number, y:number){
    const {x0,y0,layer0,layer1,pixels} = useSelector((state:RootState) => state.map)
    let i = Math.round(x/pixels - x0/pixels )  // resto porque el inicio x del mapa no esta en el centro de la pantalla
    let j = Math.round( y/pixels - y0/pixels )
    console.log(x0 + " "+ y0)
    console.log(i + " "  + j)

    if (i>=0 && j>=0) {
        return layer0[j][i].solid || layer1[j][i].solid
    }
    return true
}
