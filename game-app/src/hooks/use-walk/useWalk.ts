import { useState } from "react";
import {useDispatch, useSelector} from "react-redux";
import {setPlayerX, setPlayerY, sumPlayerX, sumPlayerY} from "../../redux/positionReducer";
import {RootState} from "../../redux/store";
export default function useWalk(maxSteps:number){
    const {playerPosition} = useSelector((state : RootState) => state.positions)
    const {x0,y0,layer0,layer1,pixels} = useSelector((state:RootState) => state.map)
    const dispatch = useDispatch()
    const [dir, setDir] = useState(0);
    const [step, setStep] = useState(0);
    const direction:any = {
        down: 0,
        left: 1,
        right: 2,
        up: 3
    }
    const stepSize = 8; // numero de pixels a moverse
    const modifier:any = {
        down: {x: 0, y: stepSize},
        left:{x: -stepSize, y:0},
        right: {x: stepSize, y: 0},
        up:{x: 0, y: -stepSize},
    }
    function stay(xStay:number,yStay:number,side:number){
        // al chocar con un solido, el player retrocede un poco y ahi se mantiene hasta moverse a otro lado
        let gap=2
        if(side === 1)
        return {
            down: {x: playerPosition.x, y: yStay-gap},
            left: {x: gap+xStay, y: playerPosition.y},
            right: {x: xStay-gap, y: playerPosition.y},
            up: {x: playerPosition.x, y: gap+yStay},
        }
        else
            return {
                down: {x: playerPosition.x, y: yStay-pixels-gap},
                left: {x: gap+pixels+xStay, y: playerPosition.y},
                right: {x: xStay-pixels-gap, y: playerPosition.y},
                up: {x: playerPosition.x, y: gap+pixels+yStay},
            }
    }
    function walk(dir:any){
        setDir(prev =>  {
            if( direction[dir] === prev) move(dir);
            return direction[dir]
        });
        setStep(x => x < maxSteps - 1? x+1: 0); // retorna al tile 0 si supera al maximo de pasos

    }
    function IsASolidTile(x:number,y:number){
        let squareY= 20 // distancia entre el punto del div del jugador al punto del cuadrado del jugador
        let squareX = 10
        let i0 = Math.floor((x+squareX)/pixels - x0/pixels )  // resto porque el inicio x del mapa no esta en el centro de la pantalla
        let j0 = Math.floor( (y+squareY)/pixels - y0/pixels )
        let iF = Math.floor((x+pixels-squareX)/pixels - x0/pixels )  // resto porque el inicio x del mapa no esta en el centro de la pantalla
        let jF = Math.floor( (y+pixels)/pixels - y0/pixels )
        if (i0>=0 && j0>=0) {
            console.log(i0 + " " +j0 + " " +iF + " " +jF + " " )
            console.log(layer0[j0][i0])
            console.log(layer1[j0][i0])
            return (layer0[j0][i0].solid || layer1[j0][i0].solid) ?
                {b:true, y:(j0+(y0/pixels))*pixels-squareY,x:(i0+(x0/pixels))*pixels-squareX,side:0} :
                (layer0[jF][iF].solid || layer1[jF][iF].solid) ?
                    {b:true, y:(jF+(y0/pixels))*pixels-pixels,x:(iF+(x0/pixels))*pixels-pixels+squareX,side:1} :
                    {b:false,y:0,x:0,side:0}
        }
        return {b:true,y:0,x:0,side:0}
    }
    function move(dir:any){
        let newX = playerPosition.x + modifier[dir].x
        let newY = playerPosition.y + modifier[dir].y
        let solidTile = IsASolidTile(newX,newY)
        if(!solidTile.b) {
            dispatch(sumPlayerX(modifier[dir].x))
            dispatch(sumPlayerY(modifier[dir].y))
        }else{
            let stayPos:any = stay(solidTile.x,solidTile.y,solidTile.side)
            dispatch(setPlayerX(stayPos[dir].x))
            dispatch(setPlayerY(stayPos[dir].y))
        }
    }
    return{
        walk, dir, step, direction
    }
}