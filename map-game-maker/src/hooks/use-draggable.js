import { useEffect, useState } from "react";

export default function useDragable(id){
    const [ position, setPosition] = useState({ x: 0, y:0});
  //  
  useEffect(() => {
    const handle = document.getElementById(id);
    handle.addEventListener("mousedown", function(e){
      e.preventDefault();
      handle.style.pointerEvents = "none"; // no se para que es :)
      document.body.addEventListener("mousemove", move);
      document.body.addEventListener("mouseup", () =>{
        document.body.removeEventListener("mousemove", move);
        handle.style.pointerEvents = "initial"; // no se para que es :)
      })
    })
    return () => {
      document.body.removeEventListener("mousedown", move) // error?
      document.body.removeEventListener("mouseup", move)
      document.body.removeEventListener("mousemove", move)
    }
  }, [ id ])
  //
  function move(e){
    const pos = { 
      x: e.clientX, // whtd is clientX?
      y: e.clientY,
    };
    setPosition(pos);
  }
  //
  return position;
}