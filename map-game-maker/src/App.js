import React, { useEffect, useState } from 'react';
import useDragable from './hooks/use-draggable';
import TilePalette from './components/tile/tile-palette';
import Map from "./components/map/map";
import useKeyPress from "./hooks/useKeyPress";
import Menu from './components/menu/Menu';
import ButtonContainer from "./components/buttonContainer/ButtonContainer.jsx";
import Layer from "./components/map/layer";
function App() {
    // states
    const [grid,setGrid] = useState(true)
    const [tileSet, setTileSet] = useState("rpg-hospital-tileset/hospital_floor_vexed");
    // layers
    const [tiles, setTiles] = useState([]);
    const [tiles2, setTiles2] = useState([])
    const [tiles3, setTiles3] = useState([])
    const [currentTile,setCurrentTile] = useState({tile: tiles, name:'layer1'})
    //
    const [mapSize, setMapSize] = useState({
        width: 800,
        height: 600,
    });
    const [activeTile, setActiveTile] = useState({ id:0, x: 0, y: 0 });
    const [activeTileMap, setActiveTileMap] = useState({id:0, x: 0, y: 0 })
    const [mapWindow,isMapWindow] = useState(true)
    const [draw, isDraw] = useState(false)
    const [solid, isSolid] = useState(false)
    const tileSize = 32
    // consts
    const [paletteSize,setPaletteSize ] = useState({width: 0,height: 0});
    // matriz useEffect
    function fillLayer(setLayer){
        const _tiles = [];
        let id = 0;
        for (let y = 0; y < mapSize.height; y += 32) {
            let row = [];
            for (let x = 0; x < mapSize.width; x += 32) {
                row.push({ x, y, id: id++,imageName: tileSet, solid: false, v: { x: -32, y: -32 } });
            }
            _tiles.push(row);
        }
        setLayer(_tiles);
    }
    useEffect(() => {
        fillLayer(setTiles)
        fillLayer(setTiles2)
        fillLayer(setTiles3)
    }, [mapSize])
    useEffect(() => {
        if(draw)
            drawMap()
    },[activeTileMap])
    useEffect(() => {
        let form = document.forms.layerForm.layerSelector.value;
        switch (form){
            case 'layer1':
                setCurrentTile({tile: tiles,  name:'layer1'})
                break;
            case 'layer2':
                setCurrentTile({tile: tiles2, name:'layer2'})
                break;
            case 'layer3':
                setCurrentTile({tile: tiles3 ,name:'layer3'})
                break;
            default:
                break;
        }
    },[tiles,tiles2,tiles3])
    // obtener dimensiones de la imagen-tiles
    useEffect(() => {
        let img = document.createElement('img');
        document.body.appendChild(img)
        img.src =`/sprites/${tileSet}.png`
        img.onload= () => setPaletteSize(() => {
            return {width: img.naturalWidth,height: img.naturalHeight}
        })
        document.body.removeChild(img)
    },[tileSet])
    //
    const position = useDragable("handle");
    // keys-press
    useKeyPress((e) => {
        if(e.key === ' ') {
            e.preventDefault()
            isMapWindow(!mapWindow)
            let div = document.getElementById('tile-palette')
            div.style.border = mapWindow ? '4px solid yellow':'none'
        }
        else if(e.key === 'Enter' && mapWindow){
            e.preventDefault()
            isDraw(!draw)
            drawMap()
        }
        else if(e.key.substring(0,5) === 'Arrow'){ //si se pulsa una tecla flecha
            e.preventDefault()
            let currentTileNumber = 0
            if (e.key === 'ArrowRight')
                currentTileNumber = 1
            else if (e.key === 'ArrowLeft')
                currentTileNumber = -1
            else if (e.key === "ArrowUp")
                    currentTileNumber =  mapWindow ? -mapSize.width / 32 : -(paletteSize.width+32)/32
            else if (e.key === 'ArrowDown')
                currentTileNumber =  mapWindow ? mapSize.width / 32 : (paletteSize.width+32)/32

            if (mapWindow) {
                setActiveTileMap((prev) => {
                    let nextId = parseInt(prev.id) + currentTileNumber + currentTile.name
                    let div = document.getElementById(prev.id)
                    if(div)
                    div.style.border = "0.5px solid black"
                    div = document.getElementById(nextId)
                    if(div) {
                        div.style.border = "solid 1px yellow"
                        return (
                            currentTileNumber === 1 || currentTileNumber === -1 ? // movimiento horizontal
                                prev.x + currentTileNumber < mapSize.width / tileSize && prev.x + currentTileNumber >= 0  ?  // no esta en los bordes?
                                    {id: parseInt(prev.id) + currentTileNumber + currentTile.name, x: prev.x + currentTileNumber, y: prev.y} :
                                    currentTileNumber === 1 ? // esta en los bordes, es movimiento derecho?
                                        {id: parseInt(prev.id) + currentTileNumber + currentTile.name, x: 0, y: prev.y + 1} :
                                        {id: parseInt(prev.id) + currentTileNumber + currentTile.name, x: mapSize.width / tileSize - 1, y: prev.y - 1}
                                :
                                currentTileNumber > 0 ? // movimiento vertical
                                    {id: parseInt(prev.id) + currentTileNumber + currentTile.name, x: prev.x, y: prev.y + 1} :
                                    {id: parseInt(prev.id) + currentTileNumber + currentTile.name, x: prev.x, y: prev.y - 1}
                        )
                    } else return prev
                })
            }
            else
                setActiveTile((prev) => {
                    let div = document.getElementById(prev.id)
                    div.style.border= '1px solid black'
                    div = document.getElementById((parseInt(prev.id) + currentTileNumber).toString())
                    if (div) {
                        div.style.border = "solid 1px yellow"
                        return (currentTileNumber === 1 || currentTileNumber === -1 ?
                                {
                                    id: parseInt(prev.id) + currentTileNumber,
                                    solid: solid,
                                    x: prev.x + 32 * currentTileNumber,
                                    y: prev.y
                                } :
                                currentTileNumber > 0 ? {
                                        id: parseInt(prev.id) + currentTileNumber,
                                        solid: solid,
                                        x: prev.x,
                                        y: prev.y + 32
                                    } :
                                    {id: parseInt(prev.id) + currentTileNumber, solid: solid, x: prev.x, y: prev.y - 32}
                        )
                    }else return prev
                })
        }
    })
    // functions
    function setTilesByLayer(layer){
        let f = () => {}
        switch(layer){
            case 'layer1':
                f =setTiles
                break;
            case 'layer2':
                f =setTiles2
                break;
            case 'layer3':
                f =setTiles3
                break;
        }
        f((prev) => {
            const imageName= document.forms.imageForm.imageSelector.value
            const clone = cloneMatrix(prev);
            const tile = {
                ...clone[activeTileMap.y][activeTileMap.x],
                id:imageName+activeTile.id,
                solid:solid,
                imageName:tileSet,
                v: activeTile,
            }
            clone[activeTileMap.y][activeTileMap.x] = tile;
            return clone;
        })
    }
    function drawMap(){
        setTilesByLayer(currentTile.name)
    }
    //
    return (
        <div className="App"
             style={{
                 position: "relative",
                 backgroundColor: "grey",
             }}
        >
            <Menu
                mapSize={ mapSize}
                setMapSize={setMapSize}
                currentTile={currentTile}
                setCurrentTile={setCurrentTile}
                layer1={tiles}
                layer2={tiles2}
                layer3={tiles3}
                grid={grid}
                setGrid={setGrid}
            />
            <TilePalette
                tileSet={tileSet}
                position={position}
                size={paletteSize}
                activeTile= {activeTile}
                setActiveTile = {setActiveTile}
                setTileSet={setTileSet}
                solid={solid}
                isSolid={isSolid}
            />
            <div style={{display:'flex',flexDirection:'row'}}>
                <Map
                    currentTile={currentTile}
                    tileSet={tileSet}
                    size={mapSize}
                    activeTile={activeTile}
                    tiles={tiles}
                    tiles2={tiles2}
                    tiles3={tiles3}
                    setTiles={setTiles}
                    setTiles2={setTiles2}
                    setTiles3={setTiles3}
                    setActiveTileMap={setActiveTileMap}
                    solid={solid}
                    grid={grid}
                />
                <ButtonContainer files={[tiles, tiles2, tiles3]} setTiles={setTiles} setTiles2= {setTiles2} setTiles3={setTiles3} />
            </div>
        </div>
    );
}
function cloneMatrix(m) {
    const clone = new Array(m.length);
    for (let i = 0; i < m.length; ++i) {
        clone[i] = m[i].slice(0);
    }
    return clone;
}

export default App;
