import {combineReducers, createStore} from "redux";
import TileReducer from "./components/tile/TileReducer";
const rootReducer = combineReducers({
    tile: TileReducer
})

const rootStore = createStore(rootReducer)
export default rootStore