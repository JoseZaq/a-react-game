import styles from './buttonContainer.module.scss'
import tilesJson from './tiles.json'
import tilesJson2 from './tiles2.json'
import tilesJson3 from './tiles3.json'
export default function ButtonContainer({files,setTiles,setTiles2,setTiles3}) {
    function handleClick(objectData){
        for(let i=0;i < objectData.length; i++) {
            let _objectData = objectData[i].map(obj =>
                obj.map(ar => {
                    let _ar = {id: ar.id, solid: ar.solid}
                    return _ar
                }))
            downloadObject(_objectData, 'layer'+ i)
        }
    }
    function downloadObject(object,name){
        let filename = name + ".json";
        let contentType = "application/json;charset=utf-8;";
        let a = document.createElement('a');
        a.download = filename;
        a.href = 'data:' + contentType + ',' + encodeURIComponent(JSON.stringify(object));
        a.target = '_blank';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }
    function handleLoadClick(){
        setTiles(tilesJson)
        setTiles2(tilesJson2)
        setTiles3(tilesJson3)
    }
    return (
        <div className={styles.buttonContainer}>
            <button onClick={() => handleClick(files)} className={styles.button}>Descargar MAPA.JSON</button>
            <button style={{background:'lightblue', color:'black'}} onClick={() => {
                for(let i=0;i< files.length;i++)
                downloadObject(files[i], 'TilesMatrix'+i)
            } }className={styles.button}>Guardar Mapa</button>
            <button style={{background:'darkgrey', color:'black'}} onClick={() => handleLoadClick()}  className={styles.button}>Cargar Mapa</button>
        </div>
    )
}
