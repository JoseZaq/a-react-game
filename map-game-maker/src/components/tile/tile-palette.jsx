import {useEffect, useState} from "react";
export default function TilePalette({ tileSet, position, size, activeTile, setActiveTile, setTileSet,solid,isSolid }) {

    const tiles = [];
    let id = 0;

    // crear el array de tiles con las dimensiones x y y
    for (let y = 0; y < size.height+32; y += 32) {
        let row = [];
        for (let x = 0; x < size.width+32; x += 32) {
            row.push({ x, y, id: id++ });
        }
        tiles.push(row);
    }
    // functions
    /**
     * Bordea de amarillo al tile elegido y actualiza el tile activo
     * @param row fila actual
     * @param x posicion x
     * @param y posicion y
     */
    function handleClick(row,x,y){
        let div = document.getElementById(y*row.length + x)
        div.style.border="solid 1px yellow"
        setActiveTile((prev) => {
            let div = document.getElementById(prev.id)
            div.style.border= '1px solid black'
            return {id: y*row.length + x ,solid:solid,x: x * 32, y: y * 32}})
    }

    // cambia de imagen
    function handleForm(){
        let form = document.forms.imageForm.imageSelector.value;
        switch(form){
            case 'floor':
                setTileSet('rpg-hospital-tileset/hospital_floor_vexed')
                break
            case 'wall':
                setTileSet('rpg-hospital-tileset/hospital_walls_vexed')
                break
            case 'stuff':
                setTileSet('rpg-hospital-tileset/hospital_stuff_vexed')
            default:
                break
        }
    // cambia el solido
    }
    function handleSolidClick(){
        isSolid(!solid)
        console.log(solid)
    }
    return (
        <div id={'tile-palette'}
             style={{
                 position: "absolute",
                 top: position.y,
                 left: position.x,
                 zIndex: 100,
                 backgroundColor: "white",
             }}
        >
            <div style={{display:'flex', justifyContent:'space-between',margin:'1% 0'}}>
                <img id="handle" src="/img/drag-handle.png" alt="" />
                <div onClick={() => handleSolidClick()}
                     style={solid ?
                         {
                             color:'white',
                             backgroundColor:'red',
                             padding:'1% 2%',
                             fontWeight:'bold',
                             boxShadow:'1px 1px 3px rgb(0 0 0 / 75%)',
                             cursor: 'pointer'
                         }
                         :
                         {
                             color:'white',
                             backgroundColor:'lightblue',
                             padding:'1% 2%',
                             fontWeight:'bold',
                             boxShadow:'1px 1px 3px rgb(0 0 0 / 75%)',
                             cursor: 'pointer'}}
                >
                    {solid ? 'Solido' : 'No Solido'}
                </div>
                <form onChange={() => handleForm()} name={'imageForm'} style={{padding: '1% 2%', display:'flex', gap:'3%'}}>
                    <label htmlFor={'image'}>Image:</label>
                    <select name={'imageSelector'}>
                        <option value={'floor'}>Floors</option>
                        <option value={'wall'}>Walls</option>
                        <option value={'stuff'}>Stuffs</option>
                    </select>
                </form>
            </div>
            <div style={{
                background: `url(/sprites/${tileSet}.png) -${activeTile.x}px -${activeTile.y}px no-repeat`,
                width: "32px",
                height: "32px",
            }}

            />
            {tiles.map((row, y) => (
                <div key={y} style={{ display: "flex" }}>
                    {row.map((tile, x) => (
                        <div id={y*row.length + x} key={x} onClick={() => handleClick(row,x,y)}
                             style={{
                                 border: "1px solid black",

                                 background: `url(/sprites/${tileSet}.png) -${x * 32}px -${y * 32}px no-repeat`,
                                 width: "32px",
                                 height: "32px",
                             }} />
                    ))}
                </div>
            ))}
        </div>
    )
}