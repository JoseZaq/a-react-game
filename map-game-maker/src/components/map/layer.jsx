import styles from './layer.module.css'

export default function Layer({ currentTile, name, tileSet, size, activeTile, setActiveTileMap,setTiles,setTiles2,setTiles3,solid, transparent, grid}) {
    function cloneMatrix(m) {
        const clone = new Array(m)
        for (let i = 0; i < m.length; ++i) {
            clone[i] = m[i].slice(0);
        }
        return clone;
    }
    function setTilesByLayer(layer,row,y,x){
        let f = () => {}
        switch(layer){
            case 'layer1':
                f =setTiles
                break;
            case 'layer2':
                f =setTiles2
                break;
            case 'layer3':
                f =setTiles3
                break;
        }
        f((prev) => {
            const image= document.forms.imageForm.imageSelector.value
            const clone = cloneMatrix(prev);
            const tile = {
                ...clone[y][x],
                id:image+activeTile.id,
                solid:solid,
                imageName: tileSet,
                v: activeTile,
            }
            clone[y][x] = tile;
            return clone;
        });
    }

    function dropTile({row,y,x}) {
        let div = document.getElementById((y*row.length + x)+name)
        div.style.border="solid 1px yellow"
        setActiveTileMap((prev) => {
            let div = document.getElementById(prev.id)
            if(div)
                div.style.border= grid ? "0.5px solid black" : "none"
            return {id:(y*row.length + x)+name,x,y}
        })
        setTilesByLayer(name,row,y,x)
    }
    return (
        <div style={ transparent ?
            {
                boxSizing: "border-box",
                backgroundColor: "transparent",
                width: size.width,
                position:'absolute',
                margin: '1vw'
            } :
            {
                boxSizing: "border-box",
                backgroundColor: "white",
                width: size.width,
                margin: '1vw'
            }
        }>
            {
                currentTile.map((row, y) =>
                    <div key={y} style={{ display: "flex" }}>
                        {row.map((tile, x) => (
                            <div
                                id={(y*row.length + x)+name} key={(y*row.length + x)+name} onClick={() => dropTile({row,y,x})}
                                className={grid ? styles.tile : styles.tileNoGrid}
                                style={{background: `url(/sprites/${tile.imageName}.png) -${tile.v?.x}px -${tile.v?.y }px no-repeat`
                                        }}>
                            </div>
                        ))}
                    </div>
                )
            }
        </div>
    )
}