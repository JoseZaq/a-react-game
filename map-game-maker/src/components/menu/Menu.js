import {useState} from "react";

export default function Menu({mapSize, setMapSize, currentTile, setCurrentTile, layer1, layer2, layer3,setLayer1,setLayer2,setLayer3,grid, setGrid}){
    //

    //functions
    function handleInput(e,measure){
        switch(measure){
            case 'WIDTH':
                setMapSize(() => {
                    return {width: e.target.value,height: mapSize.height}
                })
                break
            case 'HEIGHT':
                setMapSize(() => {
                    return {width:mapSize.width ,height: e.target.value}
                })
                break
        }
    }
    function handleForm(){
        let form = document.forms.layerForm.layerSelector.value;
        switch (form){
            case 'layer1':
                setCurrentTile({tile: layer1,  name:'layer1'})
                break;
            case 'layer2':
                setCurrentTile({tile: layer2, name:'layer2'})
                break;
            case 'layer3':
                setCurrentTile({tile: layer3 ,name:'layer3'})
                break;
            default:
                break;
        }
    }
    function handleGridButton() {
        setGrid(!grid)
        if(grid){

        }
    }

    return(
        <div style={{display:'flex', justifyContent:'space-between', backgroundColor:'lightgray', padding:'0 1%',alignItems:'center'}}>
            <p style={{textTransform:'uppercase', fontWeight:'bold',color:'darkred'}}>Map Game Maker</p>
            <form onChange={() => handleForm()} name={'layerForm'} style={{padding: '1% 2%', display:'flex', gap:'3%'}}>
                <label htmlFor={'layer'}>Capa:</label>
                <select name={'layerSelector'}>
                    <option value={'layer1'}>Capa 1</option>
                    <option value={'layer2'}>Capa 2</option>
                    <option value={'layer3'}>Capa 3</option>
                </select>
            </form>
            <div style={{display:'flex',alignItems:'center', gap:'5%'}}>
            <p>Grid:</p>
            <button onClick={() => handleGridButton()} style={grid ? {
                borderRadius:'50px',
                boxShadow:'inset 10px 0 10px rgb(0 0 0 / 75%)',
                transition: 'all linear 0.2s',
                width:'4vw'
            }:{
                borderRadius:'50px',
                boxShadow:'inset -10px 0 10px rgb(0 0 0 / 75%)',
                transition: 'all linear 0.2s',
                width:'4vw'

            }} >{grid ? 'ON' : 'OFF'}</button>
            </div>
            <div style={{display:'flex', justifyContent:'space-between', alignItems:'center',backgroundColor:'lightgray', padding:'0 2%',
                border: '1px solid black'
            }}>
                <p style={{marginRight:'5%',fontWeight:'bold'}}>Mapa</p>
                <label>Ancho:</label>
                <input type={'text'} onChange={(e) => handleInput(e,'WIDTH')}
                       value={mapSize.width}/>
                <label>Alto:</label>
                <input type={'text'} onChange={(e) => handleInput(e,'HEIGHT')}
                       value={mapSize.height}/>
            </div>
        </div>
    )
}